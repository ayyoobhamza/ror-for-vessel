#include "VesselGroup.h"

VesselGroup::VesselGroup(Ogre::SceneManager* mSceneMgr,int sessionID)
{
	srand(time(NULL));
	//GET DATA FROM DB AND INITIALIZE ALL SHIPS

	connectdb=mysql_init(NULL);


	if(!connectdb)    {
		std::cout<< "Initialization Failed"<<std::endl;

	}
	connectdb=mysql_real_connect(connectdb,DBHOST,USER,PASSWORD,DATABASE,0,NULL,0);

	if(connectdb)
	{
		std::cout<< "Connection Establised..!"<<std::endl;
	}else{

		std::cout<< "MySQL Connection Failure..!"<<std::endl;
	} 


	MYSQL_RES *res_set; 
	MYSQL_ROW row;
	mysql_select_db(connectdb,"vidusayura");

	std::stringstream ss;
	ss<<"select a.object_id as ObjectID , a.speed as velocity , b.type as shipType , b.meshName as shipmesh , b.length as length , b.width as width, b.No_of_Material as shipMat from sessions_objects a LEFT JOIN shiptypes b on a.type_id=b.ID where a.session_id='";
	ss<<sessionID;
	ss<<"';";

	if(mysql_query(connectdb,ss.str().c_str())){
		std::cout<< "Error Retrieving data1..!"<<std::endl;
	}
	else{
		//unsigned int i = 1; 
		res_set = mysql_store_result(connectdb); 
		//unsigned int numrows = mysql_num_rows(res_set);

		while ((row = mysql_fetch_row(res_set)) != NULL){

			if(row[0]!=NULL){
				int objectId = atoi(row[0]);
				float velocity=atof(row[1]);
				float length=atof(row[4]);
				float width=atof(row[5]);
				string meshName=row[3];
				string objectName=string(row[2])+string(row[0]);
				string matName=string(row[2]);
				int maxMat=atoi(row[6]);
				MYSQL_RES *res_set2; 
				MYSQL_ROW row2;


				std::stringstream ss2;
				ss2<<"select x,y,z from sessionshippaths where sessionID ='";
				ss2<<sessionID;
				ss2<<"' and objectID='";
				ss2<<objectId;
				ss2<<"'ORDER BY PathID ASC";
				//std::cout<<ss2.str().c_str()<<std::endl;
				if(mysql_query(connectdb,ss2.str().c_str())){
					std::cout<< "Error Retrieving data..!"<<std::endl;
				}

				else{


					//unsigned int j = 1; 
					res_set2 = mysql_store_result(connectdb); 
					//unsigned int numrows = mysql_num_rows(res_set);
					std::vector<Ogre::Vector3> v;
					while ((row2 = mysql_fetch_row(res_set2)) != NULL){
						if(row2[0]!=NULL){

							v.push_back(Ogre::Vector3(atof(row2[0]),atof(row2[1]),atof(row2[2])));
						} 
					}
					//ADD SHIPS TO CONTROL DATA STRUCTURE

					int rands=1 + ( rand()% maxMat);
					std::stringstream ss3;
					ss3<<rands;
					string matID=ss3.str();
					//cout<<"Mater : "<<matName<<endl;
					vesselSet.push_back(new Vessel(mSceneMgr,objectName,meshName,v,velocity,length,width,matID));

				}

				//

				mysql_free_result(res_set2);
				delete row2;


			} 
		}
	}
	delete row;
	mysql_free_result(res_set);
	mysql_close(connectdb);

	if(DEBUGMODE){
	 for(int ves=0; ves<vesselSet.size();ves++){
		vesselSet[ves]->drawPath(mSceneMgr);
	
		}
	}

}

void VesselGroup::collision_detection(Vessel * vessel1, Vessel * vessel2){

	//Collision handler


	
	Ogre::Real ang=(vessel2->getVessel()->getOrientation().getYaw()-vessel1->getVessel()->getOrientation().getYaw()).valueDegrees();

	if(ang<0) ang=ang+360;
	//
	//ship vs ship
	bool collide3=vessel2->getBoundingbox().intersects(vessel1->getBoundingbox());
	if(collide3){
		if(vessel1->getVessel()->getPosition().distance(vessel2->getVessel()->getPosition())>max(vessel1->getLength(),vessel2->getLength())){
			if(ang<=90){


				vessel2->forceSpeed(vessel1);

			}else{
				vessel1->forceSpeed(vessel2);
			}
		
		}else{

		
					vessel2->setaccident();
					vessel1->setaccident();
					vessel1->forceaccident();
					vessel2->forceaccident();
					return;
		}
	}else{
		vessel1->forceaccident();
		vessel2->forceaccident();
		
	
	}

	

	//ship vs box
	bool collide2_1=vessel2->getBoundingbox().intersects(vessel1->centerNode->_getWorldAABB());
	bool collide2_2=vessel1->getBoundingbox().intersects(vessel2->centerNode->_getWorldAABB());
	if(collide2_1 && (!collide2_2)){
		if(vessel1->getSpeed()==0||vessel2->getSpeed()==0){
		return;
	
	}
		
		for(float ij=vessel1->centerNode->_getWorldAABB().getSize().z;ij>0;ij-=10){
		if((vessel1->getVessel()->getPosition()+vessel1->getDirection()*ij).distance(vessel2->getVessel()->getPosition())<=vessel1->getWidth()*6)
		{
			vessel2->forceSpeed(vessel1);
			return;
		}else{
			if(ang<95){


				vessel1->forceSpeed(vessel2);

			}else{
				vessel2->forceSpeed(vessel1);
			}
			return;
		}
		}
		
	}else if(collide2_2&& (!collide2_1)){
		if(vessel1->getSpeed()==0||vessel2->getSpeed()==0){
		return;
	
	}
		for(float ij=vessel2->centerNode->_getWorldAABB().getSize().z;ij>0;ij-=10){
	  if((vessel2->getVessel()->getPosition()+vessel2->getDirection()*ij).distance(vessel1->getVessel()->getPosition())<=vessel2->getWidth()*6)
		{
			vessel1->forceSpeed(vessel2);
			return;
	  }else{
			if(ang<95){


				vessel2->forceSpeed(vessel1);

			}else{
				vessel1->forceSpeed(vessel2);
			}
			return;
	  
	  
	  }
		}
	 
	}else if((!collide2_2 ) && (!collide2_1)){
		//if((!collide1)&&(!collide2)&&(!collide3))
		vessel2->setDefault(vessel1);
		vessel1->setDefault(vessel2);
	}

	
	//box vs box
	bool collide1=vessel1->centerNode->_getWorldAABB().intersects(vessel2->centerNode->_getWorldAABB());
	if(collide1){


		if(ang<185 && ang >175){

			if(((vessel1->getWidth())+(vessel2->getWidth()))> getPerpendicularDistance(vessel1, vessel2)){
				//cout<<"PERPENDICULAR    "<< getPerpendicularDistance(vessel1, vessel2)<<endl;
				//cout<<"THIS IS  vessel WIDTH  "<<((vessel1->getWidth())+(vessel2->getWidth()))/2<<endl;
				////sleep(50);
				////Sleep(50);
				vessel1->forceUpdate(vessel2,vesselSet);
				vessel2->forceUpdate(vessel1,vesselSet);
				return;
			}
			//overtake
		}
		
		/*else if(){
		
		
		return;
		}*/
		
	}

	
	
	
	/*if(collide2){

		
		if(vessel1->getVessel()->getPosition().distance(vessel2->getVessel()->getPosition())<=vessel1->getLength()+vessel2->getLength()){
		if(ang<135){
			
			if(collide3){
				vessel1->setDefault(vessel2);
			}else{
			vessel1->forceSpeed(vessel2);
			}
		}else if(ang>225){
			if(collide3){
				vessel2->setDefault(vessel1);
			}else{
			vessel2->forceSpeed(vessel1);
			}

		}
		}
		

	}*/

	
	/*Ogre::Radian ship_Ang=vessel1->getDirection().angleBetween(vessel2->getDirection());
	float ship_ang_deg=Ogre::Degree(ship_Ang).valueDegrees();*/
	/*if(ship_ang_deg<0){
		ship_ang_deg=ship_ang_deg+360;
	}*/
	

	
}


float VesselGroup:: getPerpendicularDistance(Vessel * vessel1, Vessel * vessel2){

	/*
	cout<<"PERPENDICULAR    RP "<< RP<<endl;
	cout<<"PERPENDICULAR   PQ "<< PQ<<endl;*/

	/*
	P-------------------M-------------Q
	|
	|dist
	R

	*/

	Ogre::Vector3 RP((vessel1->getVessel()->getPosition().x-vessel2->getVessel()->getPosition().x),0,(vessel1->getVessel()->getPosition().z-vessel2->getVessel()->getPosition().z));
	Ogre::Vector3 PQ=vessel1->getDirection();	
	Ogre::Real lamda=(((RP.x*PQ.x)+(RP.z*PQ.z))/(PQ.x*PQ.x+PQ.z*PQ.z));
	Ogre::Vector3 m=vessel1->getVessel()->getPosition()-(lamda*PQ);

	/*cout<<"PERPENDICULAR    RP "<< RP<<endl;
	cout<<"PERPENDICULAR   PQ "<< PQ<<endl;
	cout<<"PERPENDICULAR    lamda "<< lamda<<endl;
	cout<<"PERPENDICULAR    R "<< vessel2->getVessel()->getPosition()<<endl;
	cout<<"PERPENDICULAR    M "<< m<<endl;
	cout<<"PERPENDICULAR   dist "<< vessel2->getVessel()->getPosition().distance(m)<<endl;*/
	//sleep(10);


	return vessel2->getVessel()->getPosition().distance(m);
}


bool VesselGroup:: moveVessel(const Ogre::FrameEvent& evt,Sea *viduSea)
{

	//characterNode->translate(0,0,0.01);

	
	for(int i =0; i<vesselSet.size();i++){

		for(int j =(i+1); j<vesselSet.size();j++){
			collision_detection(vesselSet[i],vesselSet[j]);

		}

	}
	
	//cout<<">>>>>>>>>>>>>vessel size<<<<<<<<<<<<<"<<vesselSet.size()<<endl;
	for(int i=0; i<vesselSet.size();i++){
		
		vesselSet[i]->animate(evt.timeSinceLastFrame,viduSea->getHydrax());
	}
	return true;
}




