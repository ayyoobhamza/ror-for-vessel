#include "Sky.h"

Sky::Sky(Camera* Camera,SceneManager *SceneMgr,RenderWindow* Window)
{
	this->mCamera= Camera;
	this->mScene = SceneMgr;
	this->mWindow = Window;
	createCaelumSky();
}

Sky::~Sky(void)
{

}
void Sky::createCaelumSky()
{
	mScene = mCamera->getSceneManager();
    mPaused = false;

        // Pick components to create in the demo.
        // You can comment any of those and it should still work
        // Trying to disable one of these can be useful in finding problems.
     Caelum::CaelumSystem::CaelumComponent componentMask;
     componentMask = static_cast<Caelum::CaelumSystem::CaelumComponent> (
                Caelum::CaelumSystem::CAELUM_COMPONENT_SUN |	
				Caelum::CaelumSystem::CAELUM_COMPONENT_SCREEN_SPACE_FOG |
                Caelum::CaelumSystem::CAELUM_COMPONENT_SKY_DOME |
                //Caelum::CaelumSystem::CAELUM_COMPONENT_IMAGE_STARFIELD |
                Caelum::CaelumSystem::CAELUM_COMPONENT_POINT_STARFIELD |
                Caelum::CaelumSystem::CAELUM_COMPONENT_CLOUDS |
                0); 
		//componentMask = Caelum::CaelumSystem::CAELUM_COMPONENTS_ALL;

        // Initialise CaelumSystem.
        mCaelumSystem = new Caelum::CaelumSystem (Root::getSingletonPtr(), mScene, componentMask);
		mCaelumSystem->setManageSceneFog(false);
		/*Ogre::ColourValue caelumFog(0.9,0.9,0.9);
		mWindow->getViewport(0)->setBackgroundColour(caelumFog);
		mCaelumSystem->setGlobalFogColourMultiplier(caelumFog);*/
		/*mCaelumSystem->setGlobalFogDensityMultiplier(0.00001); */ 
		//mCaelumSystem->getCloudSystem ()->createLayerAtHeight(2000);
        //mCaelumSystem->getCloudSystem ()->createLayerAtHeight(5000);
		//mCaelumSystem->setEnsureSingleLightGSource( true );
	/*	mCaelumSystem->getMoon()->setPhase(0);*/
		//mCaelumSystem->setMinimumAmbientLight(Ogre::ColourValue(0.5,0.5,0.5));
        // Set time acceleration.
        mCaelumSystem->getUniversalClock ()->setTimeScale (800);
		mCaelumSystem->getUniversalClock()->setGregorianDateTime(2013,10,7,06,30,00);
		//mCaelumSystem->getCloudSystem ()->getLayer(0)->setCloudSpeed(Ogre::Vector2(0.000005, -0.000009));
        // Register caelum as a listener.
        mWindow->addListener (mCaelumSystem);
        Root::getSingletonPtr()->addFrameListener (mCaelumSystem);
        UpdateSpeedFactor(mCaelumSystem->getUniversalClock ()->getTimeScale ()); 
		mCaelumSystem->setObserverLatitude(Ogre::Degree(7.31));
		mCaelumSystem->setObserverLongitude(Ogre::Degree(81.15));
		mCaelumSystem->getSun ()->setAmbientMultiplier (Ogre::ColourValue(1.0, 1.0, 1.0, 1.0));
        mCaelumSystem->getSun ()->setDiffuseMultiplier (Ogre::ColourValue(2.0, 2.0, 2.0));
		mCaelumSystem->getSun ()->setSpecularMultiplier (Ogre::ColourValue(0.1, 0.1, 0.1));
		

}
void Sky::UpdateSpeedFactor(double factor)
{
      mSpeedFactor = factor;
      mCaelumSystem->getUniversalClock ()->setTimeScale (mPaused ? 0 : mSpeedFactor);
}

void Sky::setTimeOfDay(int hours,int minutes,int seconds)
{
	mCaelumSystem->getUniversalClock()->setGregorianDateTime(2013,11,4,hours,minutes,seconds);
}

	Ogre::Vector3 Sky::getSunPosition(){
   return mCaelumSystem->getSun()->getSceneNode()->getPosition();
}

Caelum::CaelumSystem * Sky::getCaelum(){
	
		return mCaelumSystem;
}