#include <ExampleApplication.h>
#include "Sea.h"

Hydrax::Hydrax * mHydrax;



class HydraxListener : public ExampleFrameListener
{
public:
    SceneManager *mSceneMgr;
    Real mKeyBuffer;
	Sky *sky;
	Ogre::Vector3 mOriginalWaterColor;

    HydraxListener(RenderWindow* win, Camera* cam, SceneManager *sm,Sky *skySea)
            : ExampleFrameListener(win,cam)
            , mSceneMgr(sm)
            , mKeyBuffer(-1)
    {
		sky=skySea;
		mOriginalWaterColor=mHydrax->getWaterColor();
    }

    bool frameStarted(const FrameEvent &e)
    {

		
		Vector3 value =sky->getCaelum()->getSun()->getSceneNode()->_getDerivedPosition();
		ColourValue cval = sky->getCaelum()->getSun()->getBodyColour();
		mHydrax->setSunPosition(value);
		mHydrax->setSunColor(Vector3(cval.r,cval.g,cval.b));
		Ogre::Real mJulian = sky->getCaelum()->getUniversalClock()->getJulianDay();
		cval = sky->getCaelum()->getSunLightColour(mJulian,
			sky->getCaelum()->getSunDirection(mJulian));
		mHydrax->setWaterColor(Vector3(cval.r - 0.3, cval.g - 0.2, cval.b));
		Vector3 col = mHydrax->getWaterColor();
		float height = mHydrax->getSunPosition().y / 10.0f;
		Hydrax::HydraxComponent c = mHydrax->getComponents();
		if(height < 0)
		{
			if(mHydrax->isComponent(Hydrax::HYDRAX_COMPONENT_CAUSTICS))
				mHydrax->setComponents(Hydrax::HydraxComponent(
				c ^ Hydrax::HYDRAX_COMPONENT_CAUSTICS));
		} else {
			if(!mHydrax->isComponent(Hydrax::HYDRAX_COMPONENT_CAUSTICS))
				mHydrax->setComponents(Hydrax::HydraxComponent(
				c | Hydrax::HYDRAX_COMPONENT_CAUSTICS));
		}
		if(height < -99.0f)
		{
			col = mOriginalWaterColor * 0.1f;
			height = 9999.0f;
		}
		else if(height < 1.0f)
		{
			col = mOriginalWaterColor * (0.1f + (0.009f * (height + 99.0f)));
			height = 100.0f / (height + 99.001f);
		}
		else if(height < 2.0f)
		{
			col += mOriginalWaterColor;
			col /= 2.0f;
			float percent = (height - 1.0f);
			col = (col * percent) + (mOriginalWaterColor * (1.0f - percent));
		}
		else
		{
			col += mOriginalWaterColor;
			col /= 2.0f;
		}
		mHydrax->setWaterColor(col);
		mHydrax->setSunArea(height); 
		// Update Hydrax
		//mHydrax->setSunColor(getColourLinearInterpolation(0.0,mHydrax->getSunColor(), 1.0, sunTransparentColour, sky->getCaelum()->getCloudSystem()->));
		//mHydrax->setSunColor(Ogre::Vector3(sky->getCaelum()->getSun()->getBodyColour().r,sky->getCaelum()->getSun()->getBodyColour().g,sky->getCaelum()->getSun()->getBodyColour().b));
		//mHydrax->setSunPosition(sky->getCaelum()->getSun()->getSceneNode()->getPosition());
		

		//Ogre::ColourValue water_color = sky->getCaelum()->getSunLightColour( sky->getCaelum()->getUniversalClock()->getJulianDay(), sky->getCaelum()->getSunDirection( sky->getCaelum()->getUniversalClock()->getJulianDay() ) );
		//mHydrax->setWaterColor( Ogre::Vector3( water_color.r, water_color.g, water_color.b ) );
		mHydrax->update(e.timeSinceLastFrame);
		return true;
    }

	Ogre::Vector3 getColourLinearInterpolation(Ogre::Real Xa,Ogre::Vector3 Ya,Ogre::Real Xb,Ogre::Vector3 Yb,Ogre::Real X){
      const Ogre::Real k = Xa - Xb;
      Ogre::Vector3 temp;

      temp.x = ((((X-Xb)/k)*Ya.x)-(((X-Xa)/k)*Yb.x));
      temp.y = ((((X-Xb)/k)*Ya.y)-(((X-Xa)/k)*Yb.y));
      temp.z = ((((X-Xb)/k)*Ya.z)-(((X-Xa)/k)*Yb.z));
      
      return temp;
   }
   
};

Hydrax::Hydrax * Sea::getHydrax(){
	return mHydrax;

}


Sea::Sea(Ogre::SceneManager *sm, Ogre::Camera *c, Ogre::RenderWindow* w,Ogre::Root * root,Sky *skySea){


        // Create Hydrax object
	mHydrax = new Hydrax::Hydrax(sm, c, w->getViewport(0));

		mHydrax->loadCfg("vidu.hdx");
		// Create our projected grid module  
		Hydrax::Module::ProjectedGrid *mModule 
			= new Hydrax::Module::ProjectedGrid(// Hydrax parent pointer
			                                    mHydrax,
												// Noise module
			                                    new Hydrax::Noise::Perlin(/*Generic one*/),
												// Base plane
			                                    Ogre::Plane(Ogre::Vector3(0,1,0), Ogre::Vector3(0,0,0)),
												// Normal mode
												Hydrax::MaterialManager::NM_VERTEX,
												// Projected grid options
										        Hydrax::Module::ProjectedGrid::Options(/*264 /*Generic one*/));
		Hydrax::Module::ProjectedGrid::Options options = mModule->getOptions();
		options.Strength = options.Strength*10;
		// Set our module
		mHydrax->setModule(static_cast<Hydrax::Module::Module*>(mModule));
		
		// Load all parameters from config file
		// Remarks: The config file must be in Hydrax resource group.
		// All parameters can be set/updated directly by code(Like previous versions),
		// but due to the high number of customizable parameters, since 0.4 version, Hydrax allows save/load config files.
		
		
        // Create water
        mHydrax->create();

		root->addFrameListener(new HydraxListener(w, c, sm,skySea));


}