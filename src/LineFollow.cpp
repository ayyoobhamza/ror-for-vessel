
#include "LineFollow.h"





//-------------------------------------------------------------------------------------
LineFollow::LineFollow(void)
{
}
//-------------------------------------------------------------------------------------
LineFollow::~LineFollow(void)
{

}



void LineFollow::createScene(void)
{
	if(!DEBUGMODE){


		mCamera->setFarClipDistance(9999*6);
		mCamera->setPosition(27000,206.419,12000);
		mCamera->setOrientation(Ogre::Quaternion(0.998, -0.0121, -0.0608, -0.00074));
		skySea= new Sky(mCamera,mSceneMgr,mWindow);
		viduSea = new Sea(mSceneMgr, mCamera, mWindow,mRoot,skySea);
		viduBuoy=new Buoy(mSceneMgr,6);
	}else{
		mCamera->lookAt(Ogre::Vector3(2500,0,2500));
	}
	vG=new VesselGroup(mSceneMgr,6);
}

bool LineFollow::frameStarted(const Ogre::FrameEvent& evt)
{

	//Move Ships
	vG->moveVessel(evt,viduSea);
	viduBuoy->moveBuoy(evt,viduSea);

	return true;
}



#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

	//#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	//   INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
	//#else
	int main(int argc, char *argv[])
		//#endif
	{
		// Create application object
		LineFollow app;

		try {
			app.go();
		} catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occured: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif
