
#include "Character.h";
/**
 get character scene node
*/
Ogre::SceneNode* Character::getCharacter(){

	return characterNode;
}



Character::Character(Ogre::SceneManager* mSceneMgr,string entityName,string meshName,string base,float meshHeight,bool draw){
	
	entity=entityName;
	ogreHead = mSceneMgr->createEntity(entityName, meshName);
	characterNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	
	ogreHead->getSkeleton()->setBlendMode(Ogre::ANIMBLEND_CUMULATIVE);
	characterNode->attachObject(ogreHead);
	
	animateBase=base;
	baseAnim=ogreHead->getAnimationState(animateBase);
	baseAnim->setLoop(true);
	baseAnim->setEnabled(true);
	
	init=Ogre::Vector3(0,0,0);
	characterNode->setPosition(init);
	
	
	pathIndex=0;
	speed=100.0f;
	init=true;
	moveMe=true;
	mDirection = Ogre::Vector3::ZERO;
	drawArrow=draw;
	moveSideway=0;
	
	
		centerNode = characterNode->createChildSceneNode("center"+entity);
		centerNode->attachObject(mSceneMgr->createEntity("center ent"+entity,Ogre::SceneManager::PT_SPHERE));
		centerNode->setPosition(meshHeight,meshHeight/2,0);
		centerNode->setScale(0.2,0.2,0.4);
		centerNode->setPosition(meshHeight,meshHeight/2,0);
		centerNode->setVisible(false);
		
		//centerNode->showBoundingBox(true);

		//((Ogre::SceneNode*)(ch->getCharacter()->getChild("centerHead")))->showBoundingBox(true);
		leftNode = characterNode->createChildSceneNode("left"+entity);
		leftNode->attachObject(mSceneMgr->createEntity("left ent"+entity,Ogre::SceneManager::PT_SPHERE));
		leftNode->setPosition(meshHeight,meshHeight/2,-(meshHeight/2));
		leftNode->setScale(0.2,0.2,0.2);
		leftNode->setVisible(false);

		rightNode = characterNode->createChildSceneNode("right"+entity);
		rightNode->attachObject(mSceneMgr->createEntity("right ent"+entity,Ogre::SceneManager::PT_SPHERE));
		rightNode->setPosition(meshHeight,meshHeight/2,meshHeight/2);
		rightNode->setScale(0.2,0.2,0.2);
		rightNode->setVisible(false);
	
if(drawArrow){
	leftNode->showBoundingBox(true);
	centerNode->showBoundingBox(true);
	rightNode->showBoundingBox(true);
}


	


}


/**
 this method will start the individual character animation 
*/
void Character::animate(Ogre::Real offset){
	
	if(path.size()<2)return;
	//base character animation
	
	
	//this will normalize character distance with the time and velocity
	if(distance>0.0f){
		Ogre::Real move = speed* offset;
		
		if(moveMe){
			baseAnim->addTime(offset);
			if(moveSideway){
				Ogre::Quaternion rotation(Ogre::Degree(2),Ogre::Vector3::UNIT_Y);
				characterNode->translate(rotation*mDirection * move);
			}else characterNode->translate(mDirection * move);
			distance-=move;
		}
	}else{
			//this will run once the character reaches its detination point then this will give a new destination
			
			if(pathIndex+2>=path.size()){ //if there is not path to go
				//std::cout<<"end"<<std::endl;
				
				return;

			}
			pathIndex=pathIndex+1;
			//value change for each node points
			mDirection = path[pathIndex+1] - path[pathIndex];
			Ogre::Vector3 src =characterNode->getOrientation() * Ogre::Vector3::UNIT_X;
			//src.y = 0;                                                    
			mDirection.y = 0;
			src.normalise();
			distance=mDirection.normalise();
			if ((1.0f + src.dotProduct(mDirection)) < 0.0001f) 
			{
				characterNode->yaw(Ogre::Degree(180));
			}
			else
			{
				Ogre::Quaternion quat = src.getRotationTo(mDirection);
				characterNode->rotate(quat);
			}
	}
	
}


/**
 Assign a vector to the path;
*/
void Character::setPath(vector<Ogre::Vector3> pathVector){
	
	path.clear();
	for(int i=0;i<pathVector.size();i++){
		path.push_back(pathVector[i]);
	}
	
	characterNode->setPosition(path[0]);


	mDirection = path[pathIndex+1] - path[pathIndex];
	
	Ogre::Vector3 src = characterNode->getOrientation() * Ogre::Vector3::UNIT_X;      // Orientation from initial direction
	//src.y = 0;                                                    // Ignore pitch difference angle
	mDirection.y = 0;
	src.normalise();
	distance=mDirection.normalise();
	characterNode->rotate(src.getRotationTo(mDirection));


	
}

void Character::stop(){

	moveMe=false;

}
void Character::move(){

	moveMe=true;
	
}


void Character::moveSide(){

	//mDirection=mDirection+Ogre::Quaternion.
	
	/*Ogre::Quaternion rotation(Ogre::Degree(2),Ogre::Vector3::UNIT_Y); //or whatever you rotate your node by
	mDirection = rotation * mDirection; //here's your new rotated vector.
	//Ogre::Vector3 src =characterNode->getOrientation() * Ogre::Vector3::UNIT_X;
	//src.normalise();
	characterNode->rotate(rotation);*/


	/*mDirection = path[pathIndex+1] - characterNode->getPosition();
	
	Ogre::Vector3 src = characterNode->getOrientation() * Ogre::Vector3::UNIT_X;      // Orientation from initial direction
	//src.y = 0;                                                    // Ignore pitch difference angle
	mDirection.y = 0;
	src.normalise();
	distance=mDirection.normalise();
	characterNode->rotate(src.getRotationTo(mDirection));*/
	moveSideway=moveSideway+40;
}


Ogre::AxisAlignedBox Character::getBoundingbox(){
	
	Ogre::AxisAlignedBox box= characterNode->_getWorldAABB();
	//std::map<int,std::vector<int>> n;
	//auto x=n.find(1);
	//x->second.
	return box;

}
/**
 Assign a new path while running
*/
void Character::setPath(Ogre::Vector3 newPath){

	path.push_back(newPath);

}

void Character::drawPath(Ogre::SceneManager* mSceneMgr){
	if(path.size()<2)return;
	Ogre::ManualObject* myManualObject =  mSceneMgr->createManualObject("manual1"+entity); 
	Ogre::SceneNode* myManualObjectNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("manual1_node"+entity); 
 
	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
		Ogre::MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manual1Material"+entity,"General"); 
		myManualObjectMaterial->setReceiveShadows(false); 
		myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true); 
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(0,0,1,0); 
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setAmbient(0,0,1); 
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1); 
		//myManualObjectMaterial->dispose();  // dispose pointer, not the material
 
 
		for(int i =1;i <path.size();i++){
			myManualObject->begin("manual1Material"+entity, Ogre::RenderOperation::OT_LINE_LIST); 
			myManualObject->position(path[i-1]); 
			myManualObject->position(path[i]);
			myManualObject->end(); 
		
		}
 
myManualObjectNode->attachObject(myManualObject);

	
 



}







