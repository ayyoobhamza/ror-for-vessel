
#include "vessel.h";
/**
get character scene node
*/


Ogre::SceneNode* Vessel::getVessel(){

	return vesselNode;
}



Vessel::Vessel(Ogre::SceneManager* mSceneMgr,string entityName,string meshName,vector<Ogre::Vector3> pathVector,float safeSpeed , float length , float width,string matID){

	//CREATE A SHIP NODE
	
	entity=entityName;
	vesselEntity = mSceneMgr->createEntity(entityName, meshName);
	vesselNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	vesselNode->attachObject(vesselEntity);	
	noOfForcePath=0;
	//cout<<"TEXTURE YAKO :D"<<vesselEntity->getSubEntity(0)->getTechnique()->getPass(0)->getTextureUnitState(0)->getTextureName()<<endl;
	int num=vesselEntity->getNumSubEntities();


	for(int i=0;i<num;i++){

		Ogre::MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(vesselEntity->getSubEntity(i)->getMaterialName()+matID,"General"); 

		myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(vesselEntity->getSubEntity(i)->getTechnique()->getPass(0)->getDiffuse()); 
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setAmbient(vesselEntity->getSubEntity(i)->getTechnique()->getPass(0)->getAmbient()); 
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(vesselEntity->getSubEntity(i)->getTechnique()->getPass(0)->getSpecular()); 

		Ogre::Pass::TextureUnitStateIterator itr=vesselEntity->getSubEntity(i)->getTechnique()->getPass(0)->getTextureUnitStateIterator();
		//vesselEntity->getSubEntity(i)->getTechnique()->getPass(0)->getTextureUnitState(0)->getTextureName()
		int index=0;
		while(itr.hasMoreElements()){
			string s=itr.getNext()->getTextureName();
			vector<string>v;

			split(s,'.',v);
			if(v.size()==2){
				stringstream ss;
				ss<<v[0];
				ss<<matID;
				ss<<".";
				ss<<v[1];		
				myManualObjectMaterial->getTechnique(0)->getPass(0)->createTextureUnitState(ss.str(),index);
				index++;
			}

		}
		vesselEntity->getSubEntity(i)->setMaterialName(vesselEntity->getSubEntity(i)->getMaterialName()+matID);


	}
	cout<<">>>   "<<entity<<"  > >>>>   tESTTT  "<<path.size() <<endl;
	ratio=length/width;
	shift=0;

	speed=safeSpeed;
	defaultSpeed=speed;

	mDirection = Ogre::Vector3::ZERO;
	//control ship collision structure 
	centerNode = vesselNode->createChildSceneNode("center"+entityName);
	centerNode->attachObject(mSceneMgr->createEntity("center ent"+entityName,Ogre::SceneManager::PT_CUBE));
	centerNode->setScale(1,0.5,ratio/2);
	centerNode->setVisible(false);
	//centerNode->setPosition(vesselNode->getPosition());
	centerNode->translate(0,0,-(length*ratio)/2);
	myWay=GOING_STRAIGT;
	if(DEBUGMODE){

		centerNode->showBoundingBox(true);
		vesselNode->showBoundingBox(true);
	}


	
	mRotating=false;
	forcedirection=false;
	collideVessel=this;
	accident=false;
	shipWidth=width;
	shipLength=length;
	setPath(pathVector);
}


/**
this method will start the individual character animation 
*/
void Vessel::animate(Ogre::Real offset,Hydrax::Hydrax * mHydrax){
	
	if(path.size()<1)return;
	//base character animation


	//this will normalize character distance with the time and velocity
	if(mRotating)                                // Process timed rotation
	{
		mRotProgress += mRotFactor;
		if(mRotProgress>1)
		{
			mRotating = false;
		}
		else
		{
			// Ogre::Quaternion delta = Ogre::Quaternion::Slerp(mRotProgress, mOrientSrc, mOrientDest, true);
			Ogre::Quaternion delta = Ogre::Quaternion::Squad(mRotProgress, mOrientSrc, mOrientIntA, mOrientIntB, mOrientDest, true);
			vesselNode->setOrientation(delta);
		}
	}
	if(distance>0.0f&&(!forcedirection)){
		
		Ogre::Real move = speed* offset;
		//std::cout<<"end path"<<path[pathIndex]<<std::endl;
		vesselNode->translate(mDirection * move);
		
			distance-=move;

	}else{
		//this will run once the character reaches its destination point then this will give a new destination

		if(path.size()<=1){ //if there is not path to go
				//std::cout<<"NO PATH TO GO"<<path.size()<<std::endl;
			

		}else{
		
		path.pop_front();
		if(forcedirection && noOfForcePath>0){
			
			noOfForcePath--;
		}
		
		if(!forcedirection && (noOfForcePath<=0)){
			currentTarget=path.front();
		
		}
		if(noOfForcePath==0)myWay=GOING_STRAIGT;
		forcedirection=false;
		//value change for each node points
		mDirection = path.front() -vesselNode->getPosition();
		Ogre::Vector3 src =vesselNode->getOrientation() * Ogre::Vector3::UNIT_Z*-1;
		//src.y = 0;                                                    
		mDirection.y = 0;
		src.normalise();
		distance=mDirection.normalise();
		
		if ((1.0f + src.dotProduct(mDirection)) < 0.0001f) 
		{
			vesselNode->yaw(Ogre::Degree(180));
		}
		else
		{
			Ogre::Quaternion quat = src.getRotationTo(mDirection);
			//vesselNode->rotate(quat);

			mRotating = true;
			mRotFactor = 1.0f / (speed);
			mOrientSrc = vesselNode->getOrientation();
			mOrientDest = quat * mOrientSrc;           // We want dest orientation, not a relative rotation (quat)
			mOrientIntA = Ogre::Quaternion::Slerp(.3, mOrientSrc, mOrientDest, true);
			mOrientIntB = Ogre::Quaternion::Slerp(.5, mOrientSrc, mOrientDest, true);
			mRotProgress = 0;
		}
		
		}

	}
	if(!DEBUGMODE){
			vesselNode->setPosition(vesselNode->getPosition().x,mHydrax->getHeigth(Ogre::Vector2(vesselNode->getPosition().x,vesselNode->getPosition().y))/3,vesselNode->getPosition().z);
	}else{
		vesselNode->setPosition(vesselNode->getPosition().x,0,vesselNode->getPosition().z);
	}
}

void Vessel::forceSpeed(Vessel *c){
	if(speed>0)speed=speed-0.05;
	lockVessel=c;
	
}

void Vessel::forceBack(Vessel *c){
	
	path.push_front(c->getVessel()->getPosition()+c->getDirection()*(c->getLength()*-1));
	noOfForcePath=noOfForcePath+1;
	
}
void Vessel::setDefault(Vessel *c){
	if(lockVessel==c && !accident ){
			if(speed>defaultSpeed){
				speed=defaultSpeed;
			}else{
				speed=speed+0.05;
			
			}	
	}
}
void Vessel::setaccident(){
	if(speed>0)speed=speed-0.05;
	accident=true;
}

void Vessel::forceaccident(){
	/*if(accident){
		
		if(vesselNode->getOrientation().getPitch()<Ogre::Radian(Ogre::Degree(45))){
			std::cout<<entity<<"  >>> this pitch ???   "<<Ogre::Degree(vesselNode->getOrientation().getPitch())<<std::endl;
			Sleep(500);			
			vesselNode->pitch(Ogre::Radian(Ogre::Degree(0.1)),Ogre::Node::TS_LOCAL);
						vesselNode->translate(0,-0.5,0);
		}
		
	}*/
	
	
}

void Vessel::forceUpdate(Vessel *c,vector<Vessel *> vesselset){

	//check wheter we can move to the target -->chk for collision
	float move=(shipWidth+c->getWidth())*20;
	
	if(collideVessel==c){

		shift+=move;
	}else{

		shift=move;
	
	}
	int density=0;
		for(int j =0; j<vesselset.size();j++){
			if(vesselset[j]->getVessel()==vesselNode){
				continue;
			}
			/*cout<<">>>>>>>distew>>>>>>>> "<<(vesselNode->getPosition()+getDirection().perpendicular()*shift).distance(vesselset[j]->getVessel()->getPosition())<<endl;
			cout<<">>>>>>>shift>>>>>>>> "<<shift<<endl;*/
			if((vesselNode->getPosition()+getDirection().perpendicular()*shift).distance(vesselset[j]->getVessel()->getPosition())<=2*shift){
				density++;
			}
		}
	
//	cout<<">>>>>>>>>>>>>>>DENSITY  "<<density<<endl;
	//Sleep(5000);
	if(density==0){
	float tmove=(shipWidth+c->getWidth())*10;
	if(collideVessel==c){

		shift-=move;
		shift+=tmove;
	}else{

		
		
		shift=tmove;
	
	}
	}
	//cout<<" MAGULA  "<<shift<<endl;
	//Sleep(1000);


	if(forcedirection){
		
		for(int i=noOfForcePath;i>0;i--){

			path.pop_front();
			
		}
		noOfForcePath=0;
		
	}
	
	myWay=TURN_RIGHT;
	
	Ogre::Vector3 v2=c->getVessel()->getPosition();
	//forcedShipPosition=v2;
	
	 
	//calculate the next point to move in case of collision
	vector<Ogre::Vector3> tmp;
	Ogre::Vector3 v1=vesselNode->getPosition();
	bool last=false;
	float val=shift;
	float dist=v1.distance(v2);
	float d= sqrt((val*val)+(dist*dist));
	if(dist>=distance+shipLength){
		///TODO : chk the condition turn before right TURN
		
		return;

	}else{

		if((ratio*shipLength+dist)<distance){
			//back to coarse
			
			tmp.push_back(v1+((ratio*shipLength+dist)*mDirection));
			noOfForcePath=noOfForcePath+1;
		}else{
			tmp.push_back(path.front());
			last=true;
		}

	}
	Ogre::Radian angle=Ogre::Math::ATan((val/dist));
	Ogre::Quaternion q1(-angle,Ogre::Vector3::UNIT_Y);
	Ogre::Vector3 directionVec1= q1* mDirection ;
	Ogre::Vector3 choosePoint1=v1+d*directionVec1;
	//NEXT POINT choosePOint is created ^

	collideVessel=c;
	forcedirection=true;

	tmp.push_back(choosePoint1);
	tmp.push_back(vesselNode->getPosition());

	vector<Ogre::Vector3> bez=getBezierPoint(tmp[2],tmp[1],tmp[0],0.05);

	//noOfForcePath=noOfForcePath+2+bez.size();
	noOfForcePath=noOfForcePath+2;
	if(!last){
		path.push_front(tmp[0]);	
	}
	
	/*if(bez.size()>0){
		for(int h=0; h<bez.size();h++){
		path.push_front(bez[h]);
		
		}
	
	}else{*/
		path.push_front(tmp[1]);
	
	//}
	path.push_front(tmp[2]);
	
	//}
	

}

Ogre::Vector3 Vessel::getCurrentTarget(){
	return path.front();
	
	//return currentTarget;
}

/**
Assign a vector to the path;
*/
void Vessel::setPath(vector<Ogre::Vector3> pathVector){

	//cout<<"SIZE        ??????  "<<pathVector.size()<<"  "<<endl;
	path.clear();
	coursepath.clear();

	

	for(int i=0;i<pathVector.size();i++){

		
		if(i==0 || i==(pathVector.size()-1)){
		
			path.push_back(pathVector[i]);

			coursepath.push_back(pathVector[i]);
		}else{
			
			vector<Ogre::Vector3> curvePath=getBezierPoint(pathVector[i-1],pathVector[i],pathVector[i+1],0.01);
			//cout<<"HERE I M Ayyoob      ???????????  "<<curvePath.size()<<endl;
			for(int j=0;j<curvePath.size();j++){
				
				path.push_back(curvePath[j]);
				coursepath.push_back(curvePath[j]);
				
			}
		}
		
		
	}
	
	Ogre::Vector3 start=path.front();
	path.pop_front();
	currentTarget=path.front();

	vesselNode->setPosition(start);
	mDirection = currentTarget - start;
	Ogre::Vector3 src =vesselNode->getOrientation() * Ogre::Vector3::UNIT_Z*-1;
	//src.y = 0;                                                    
	mDirection.y = 0;
	src.normalise();
	distance=mDirection.normalise();
	if ((1.0f + src.dotProduct(mDirection)) < 0.0001f) 
	{
		vesselNode->yaw(Ogre::Degree(180));
	}
	else
	{
		Ogre::Quaternion quat = src.getRotationTo(mDirection);
		vesselNode->rotate(quat);
	}



}

vector<Ogre::Vector3> Vessel::getBezierPoint(Ogre::Vector3 P0,Ogre::Vector3 P1,Ogre::Vector3 P2,double stepSizeT){
	//B(t)=(1-t)^2*P0+2(1-t)t*P1+t^2*P2      0<=t<=1
	
	bool changeP2=false;
	vector<Ogre::Vector3> bezierPoints;
	Ogre::Vector3 P0_1=(P1-P0);
	float P0_1_dist=P0_1.normalise();

	


	Ogre::Vector3 P1_2=(P2-P1);
	float P1_2_dist=P1_2.normalise();

	/*cout<<"DIRECTION 0-->1 " <<P0_1<<endl;
	cout<<"DISTANCE  0-->1 " <<P0_1_dist<<endl;
	cout<<"                    " <<endl;
	cout<<"DIRECTION 1-->2 " <<P1_2<<endl;
	cout<<"DISTANCE  1-->2 " <<P1_2_dist<<endl;
	cout<<"LENGTH  " <<getLength()*20<<endl;*/
	if(P0_1_dist>(getLength()*5)){
		P0=P1-(P0_1*(getLength()*5));
		bezierPoints.push_back(P0);
	}

	if(P1_2_dist>(getLength()*5)){
		
		P2=P1+(P1_2*(getLength()*5));
		changeP2=true;
	}

	/*cout<<"P0   >>>  " <<P0<<endl;
	cout<<"P2   >>>  " <<P2<<endl;*/
	
	if(stepSizeT>1 || stepSizeT <0){stepSizeT =0.1;}
	
	for(float t=stepSizeT;t<1;t=t+stepSizeT){
		float x=((1-t)*(1-t))*P0.x+2*(1-t)*t*P1.x+t*t*P2.x;
		float z=((1-t)*(1-t))*P0.z+2*(1-t)*t*P1.z+t*t*P2.z;
		bezierPoints.push_back(Ogre::Vector3(x,0,z));
		//cout<<"PATH   >>>    " <<Ogre::Vector3(x,0,z)<<endl;
	}

	if(changeP2){
		bezierPoints.push_back(P2);
	
	}

	return bezierPoints;

}

Ogre::Quaternion Vessel::getDesriedOrientation(){
	//currentTarget-vesselNode->getPosition()
	return vesselNode->getOrientation();

}

float Vessel::getWidth(){
	return shipWidth;
}
float Vessel::getLength(){
	return shipLength;
}
float Vessel::getSpeed(){
	return speed;
}


Ogre::Vector3 Vessel::getDirection(){
	return mDirection;
}

Vessel::signal Vessel::getWay(){

	return myWay;

}



Ogre::AxisAlignedBox Vessel::getBoundingbox(){

	Ogre::AxisAlignedBox box= vesselNode->_getWorldAABB();

	return box;

}
/**
Assign a new path while running
*/
void Vessel::setPath(Ogre::Vector3 newPath){

	path.push_back(newPath);

}

void Vessel::drawPath(Ogre::SceneManager* mSceneMgr){
	if(coursepath.size()<2)return;
	
	Ogre::ManualObject* myManualObject =  mSceneMgr->createManualObject("manual1"+entity); 
	Ogre::SceneNode* myManualObjectNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("manual1_node"+entity); 

	// NOTE: The second parameter to the create method is the resource group the material will be added to.
	// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
	Ogre::MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create("manual1Material"+entity,"General"); 
	myManualObjectMaterial->setReceiveShadows(false); 
	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true); 
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(0,0,1,0); 
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setAmbient(0,0,1); 

	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1); 
	//myManualObjectMaterial->dispose();  // dispose pointer, not the material


	for(int i =1;i <coursepath.size();i++){
		myManualObject->begin("manual1Material"+entity, Ogre::RenderOperation::OT_LINE_LIST); 
		myManualObject->position(coursepath[i-1]); 
		myManualObject->position(coursepath[i]);
		
		myManualObject->end(); 

	}

	myManualObjectNode->attachObject(myManualObject);


}







