#include "util.h"

void split(std::string str, char delim, std::vector<std::string>& strvec)
{
    char *sptr=(char*)str.c_str(),*eptr=(char*)str.c_str();
    while (*eptr!='\0'){
        if (*eptr == delim){
            char tmp = *eptr;
            *eptr='\0';
            if (*sptr != '\0')
                strvec.push_back( std::string(sptr) );
            *eptr=tmp;
            sptr=eptr+1;
        }
        eptr++;
    }
    if (*sptr != '\0')
        strvec.push_back( std::string(sptr) );
}