

#ifndef __Buoy_
#define __Buoy_
#include <sstream>
#include "Ogre.h"
#include <string>
#include <vector>
#include "database.h"
#include "Sea.h"

class Buoy{
public:
    Buoy(Ogre::SceneManager* mSceneMgr,int sessionID);
    ~Buoy(void);
	bool moveBuoy(const Ogre::FrameEvent& evt, Sea *viduSea);
	
	MYSQL* connectdb;
	

protected:
	std::vector<Ogre::SceneNode *> buoySet;
};

#endif // #ifndef __LineFollow_h_
