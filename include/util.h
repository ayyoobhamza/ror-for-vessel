
#ifndef _UTIL_
#define _UTIL_
#include <string>
#include <vector>

void split(std::string str, char delim, std::vector<std::string>& strvec);

#endif