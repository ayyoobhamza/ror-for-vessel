
#ifndef __VIDUSEA_
#define __VIDUSEA_


#include "Hydrax/Hydrax.h"
#include "Hydrax/Noise/Perlin/Perlin.h"
#include "Hydrax/Modules/ProjectedGrid/ProjectedGrid.h"
#include "sky.h"

class Sea 
{
public:
	Sea(Ogre::SceneManager *sm, Ogre::Camera *c, Ogre::RenderWindow* w,Ogre::Root *root,Sky *skySea);
	Hydrax::Hydrax * getHydrax();
    ~Sea(void);
	Sky *skySea;
	
protected:
    
	
};

#endif // #ifndef __VIDUSEA_
