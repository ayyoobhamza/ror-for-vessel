
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreAnimationState.h>
#include "timer.h"
#include <OgreManualObject.h>
using namespace std;

class Character
{
	private:
		Ogre::SceneNode* characterNode;
		Ogre::Entity* ogreHead;
		string animateBase;
		Ogre::AnimationState* baseAnim;
		Ogre::Vector3 init;
		vector<Ogre::Vector3> path;
		
		string entity;
		int pathIndex;
		float speed;
		timestamp_t stepTime;
		Ogre::Real distance;
		timestamp_t startTime;
		bool firstRun;
		Ogre::Vector3 mDirection;
		bool moveMe;
		Ogre::Vector3 oldPath;
		bool drawArrow;
		int moveSideway;

	public:
		Ogre::SceneNode* getCharacter();
		Character(Ogre::SceneManager* mSceneMgr,string entityName,string meshName,string base,float meshHeight,bool draw=false);
		void animate(Ogre::Real offset);
		void setPath(Ogre::Vector3 newpath);
		void setPath(vector<Ogre::Vector3> pathVector);
		Ogre::AxisAlignedBox getBoundingbox();
		void drawPath(Ogre::SceneManager* mSceneMgr);
		void stop();
		void move();
		void moveSide();
		Ogre::SceneNode* leftNode;
		Ogre::SceneNode* rightNode;
		Ogre::SceneNode* centerNode;
		
};