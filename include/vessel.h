
#include <OgreEntity.h>
#include <OgreSubEntity.h>
#include <OgreSceneManager.h>
#include <OgreAnimationState.h>
#include <OgreManualObject.h>
#include "util.h"
#include "Hydrax/Hydrax.h"
#define DEBUGMODE 0
using namespace std;

class Vessel
{
	private:
		Ogre::SceneNode* vesselNode;
		Ogre::Entity* vesselEntity;
		
		std::list<Ogre::Vector3> path;
		std::vector<Ogre::Vector3> coursepath;
	
		string entity;
		float speed;
		float defaultSpeed;
		Ogre::Vector3 mDirection;

		
		Ogre::Real distance;
		
		Ogre::Vector3 forcedShipPosition;
		int noOfForcePath;
		bool forcedirection;
		Ogre::Vector3 oldPath;
		Vessel *collideVessel;
		Vessel *lockVessel;
		//set smooth rotation
		Ogre::Quaternion mOrientSrc;               // Initial orientation
		Ogre::Quaternion mOrientDest;              // Destination orientation
		Ogre::Real mRotProgress;                   // How far we've interpolated
		Ogre::Real mRotFactor;                     // Interpolation step
		bool mRotating;
		Ogre::Quaternion mOrientIntA;
		Ogre::Quaternion mOrientIntB;
		float shipLength;
		float shipWidth;
		float ratio;
		float shift;
		bool accident;
		Ogre::Vector3 currentTarget;
		enum signal{
			GOING_STRAIGT,
			TURN_LEFT,
			TURN_RIGHT,
		
		} myWay;
	public:
		
		Ogre::SceneNode* getVessel();
		Vessel(Ogre::SceneManager* mSceneMgr,string entityName,string meshName,std::vector<Ogre::Vector3> pathVector,float safeSpeed , float length , float width, string matID);
		void animate(Ogre::Real offset, Hydrax::Hydrax * mHydrax);
		void setPath(Ogre::Vector3 newpath);
		void setPath(std::vector<Ogre::Vector3> pathVector);
		Ogre::AxisAlignedBox getBoundingbox();
		void drawPath(Ogre::SceneManager* mSceneMgr);
		Ogre::SceneNode* centerNode;
		void forceUpdate(Vessel *c,std::vector<Vessel *> vesselset);
		void forceBack(Vessel *c);
		void forceSpeed(Vessel *c);
		void setDefault(Vessel *c);
		Ogre::Vector3 getDirection();
		Ogre::Vector3 getCurrentTarget();
		float getWidth();
		float getLength();
		float getSpeed();
		Ogre::Quaternion getDesriedOrientation();
		signal getWay();
		void setaccident();
		void forceaccident();
		std::vector<Ogre::Vector3> getBezierPoint(Ogre::Vector3 P0,Ogre::Vector3 P1,Ogre::Vector3 P2,double stepSizeT);
};

