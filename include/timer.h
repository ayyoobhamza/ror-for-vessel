#ifndef TIMER_H
#define TIMER_H

#ifdef _WIN32
#include <windows.h>
typedef ULONGLONG timestamp_t;
namespace std{
static ULONGLONG last=0LL;
class timer{
public:
    static timestamp_t get_time()
    {
        DWORD now = GetTickCount();

        if ((last & 0xFFFFFFFFL) > now) {
            last += 0x100000000LL | now;
        } else {
            last = now;
        }
        return last;
    }
    static void sleep(unsigned millis)
    {
        Sleep((DWORD)millis);
    }
};
}
#elif __APPLE__
#include <sys/time.h>
#include <stdint.h>
#include <unistd.h>
typedef uint64_t timestamp_t;
namespace std{
class timer{
public:
    static inline timestamp_t get_time()
    {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        return tv.tv_sec*1000+tv.tv_usec/1000;
    }
    static inline void sleep(unsigned millis)
    {
        usleep(millis*1000);
    }
};
}
#else
#include <stdint.h>
#include <time.h>
#include <unistd.h>
typedef uint64_t timestamp_t;
namespace std{
class timer{
public:
    static inline timestamp_t get_time()
    {
        timespec t;

        clock_gettime(CLOCK_BOOTTIME,&t);
        return t.tv_sec*1000 + t.tv_nsec/1000000;
    }
    static inline void sleep(unsigned millis)
    {
        usleep(millis*1000);
    }
};
}
#endif
#endif // TIMER_H