#pragma once

#include "Caelum.h"
#include "BaseApplication.h"
#include "ExampleApplication.h"

using namespace Ogre;
class Sky
{
public:
	Sky(Camera* Camera,SceneManager *SceneMgr,RenderWindow* Window);
	~Sky(void);
	void UpdateSpeedFactor(double factor);
	void createCaelumSky();
	void setTimeOfDay(int hours,int minutes,int seconds);
	Ogre::Vector3 Sky::getSunPosition();
	Caelum::CaelumSystem * getCaelum();
private:
	
	Caelum::CaelumSystem *mCaelumSystem;
    Camera* mCamera;
	SceneManager *mScene;
	RenderWindow* mWindow;
    float mSpeedFactor;
    bool mPaused;
};
