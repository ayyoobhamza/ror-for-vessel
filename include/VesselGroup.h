
#ifndef __VesselGrouph_
#define __VesselGrouph_
#include <sstream>
#include "vessel.h"
#include "database.h"
#include "Sea.h"
#include<ctime>
#include<cstdlib>



class VesselGroup{
public:
    VesselGroup(Ogre::SceneManager* mSceneMgr,int sessionID);
    ~VesselGroup(void);
	bool moveVessel(const Ogre::FrameEvent& evt, Sea *viduSea);
	void collision_detection(Vessel * vessel1, Vessel * vessel2);
	void response();
	MYSQL* connectdb;
	float getPerpendicularDistance(Vessel * vessel1, Vessel * vessel2);

protected:
	std::vector<Vessel *> vesselSet;
};

#endif // #ifndef __LineFollow_h_
